<?php
/*
 * Plugin Name: Training Plugin
 * Plugin Name: Training Plugin
Plugin URI: http://secretlab.pw/training/
Description: Lets do something awesome!
Version: 1.0.0
Author: SecretLab
Author URI: http://secretlab.pw/
Text Domain: traning-plugin
Domain Path: /languages
 */

add_action('admin_notices', function(){
    echo '<div class="notice notice-success is-dismissible"><p>Это безполезный плагинт для вывода картинок</p></div>';
});

add_action('admin_menu', 'tp_add_plugin_page');
function tp_add_plugin_page(){
    add_menu_page( 'Тренинг плагин',
        'Тренинг',
        'manage_options',
        'my_slug',
        'tp_primer_options_page_output',
        'dashicons-editor-spellcheck',
    '6'
        );
}
function tp_primer_options_page_output() {
    echo '<h1>Заголовок нашей страницы</h1>';
    echo '<p>Some description here</p>';
    ?>
    <form action="options.php" method="POST">
			<?php
				settings_fields( 'my_group' );     // скрытые защитные поля
				do_settings_sections( 'my_slug' ); // секции с настройками (опциями). У нас она всего одна 'section_id'
				submit_button();
			?>
    </form>
    <?php
}
add_action('admin_init','tp_image_options');
function tp_image_options() {
    register_setting( 'my_group', 'tp_section_list' );
    add_settings_section(
        'tp_first_section',
        'Первая секция настроек',
        'tp_first_callback',
        'my_slug'
    );


    // добавляем поле
    add_settings_field(
        'tp_setting',
        'Название опции',
        'tp_setting_callback_function',
        'my_slug',
        'tp_first_section'
    );
}
function tp_first_callback() {
    echo '<p>Описание первой секции натсроек</p>';
}

function tp_setting_callback_function(){
    $val = get_option('tp_section_list');
    $val = $val ? $val['tp_setting'] : null;
    ?>
    <input type="text" name="tp_section_list[tp_setting]" value="<?php echo esc_attr( $val ) ?>" />
    <?php
}
// Add Shortcode [image url="" width="" height=""]
function tp_image_custom_shortcode( $atts ) {
    $url = $width = $height = '';
    // Attributes
    extract($atts = shortcode_atts(
        array(
            'url' => '',
            'width' => '400',
            'height' => '',
        ),
        $atts,
        'image'
    ));
    $out = '<img src="'.$url.'" alt="" width="'.$width.'" height="'.$height.'">';
    return $out;
}
add_shortcode( 'image', 'tp_image_custom_shortcode' );